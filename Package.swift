// swift-tools-version:5.0
import PackageDescription


let package = Package(
	name: "test_libdispatch_linux",
	products: [
		.executable(name: "test_libdispatch_linux", targets: ["test_libdispatch_linux"]),
		.library(name: "test_libdispatch_linux_lib", targets: ["test_libdispatch_linux_lib"])
	],
	dependencies: [
	],
	targets: [
		.target(name: "test_libdispatch_linux_lib", dependencies: []),
		.target(name: "test_libdispatch_linux", dependencies: ["test_libdispatch_linux_lib"]),
		.testTarget(name: "Test", dependencies: ["test_libdispatch_linux_lib"])
	]
)
